TUTORIAL End : Taking a copy of files and data
==============================================

Author James Grant, r.j.grant{at}bath.ac.uk

Back-up
-------

If you wish to take a copy of data, the tutorials or input files the easiest way is to copy the files to your $BUCSHOME::

[user@host.domain directory] cp file $BUCSHOME

the file(s) should then appear in the H: drive and from there copied on to a USB.Alternatively you can copy these directly to a machine that you are able to access via ssh.

In the case of the inputs and outputs from the tutorials a compressed .tar.gz are available for the files used in the workshop are available from $TARS::

[user@host.domain directory] cp $TARS/* $BUCSHOME

If you wish to take a copy of any of the data you have produced please do so but note that the $BUCSHOME space is usually limited to 1GB of data, so if you wish to transfer a large amount you may need to do so in stages.

If you are unable to take copies today, the data will be preserved on the Balena system after the workshop and with your permission I will be able to forward this on to you at a later date.


