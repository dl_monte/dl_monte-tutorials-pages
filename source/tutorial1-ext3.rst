.. _tut1_ex3:

TUTORIAL 1 : Exercise 3 
=======================

Ex. 1.3  Move size update
-------------------------

DL_MONTE is able to automatically tune the size of attempted moves to optimise performance.
If proposed moves are very small then energy differences between successive configurations will be small and typically accepted, however the system will evolve very slowly.
Conversely if the proposed moves are very large then the energy differences between will generally be large and rejected.
By altering the maximum proposed move size during the simulation DL_MONTE is able to optimise for the particular problem.
The two parts of code are generating the move and updating the move size.
Example pseudo code for generating the move is:

.. code-block:: html
   :linenos:
   
   atm = random_number * natoms + 1
   delta_pos = (random_number - 0.5) * max_atm_displacement
   pos_new = pos_old + delta_pos 

The maximum displacement of an atom is controlled by the variable *max_atm_displacement*. 
The *max_atm_displacement* can not be known prior to the start of the simulation and the most suitable valuable alters as the simulation progresses. 
THe acceptance ratio (ratio of accepted moves to rejected moves) can determine the rate of equilibration and the efficiency of the sampling.
For these reasons DL_MONTE provides a mechanism for adjusting the value of *max_atm_displacement* as the simulation proceeds.
The mechanism for doing this is given in the pseudocode: 

.. code-block:: html
   :linenos:
   
    do iter = 1 to max_iterations
    
        DO MONTE CARLO STUFF
    
        if mod(iter / accept_atm_move_update) == 0
        
            ratio = accepted_moves / attempted_moves
            
            if ratio > accept_atm_move_ratio
            
                max_atm_displacement = max_atm_displacement * 1.05
                
            else
            
                max_atm_displacement = max_atm_displacement * 0.95
                
            endif
            
        endif
        
    enddo

There are three parameters that need to be given with the relevant directives in the CONTROL file::

    maxatmdist   0.1
    acceptatmmoveupdate      100
    acceptatmmoveratio    0.37

Here we provide the default values but by inserting the directives and exploring the values you can see how using the optimum values can improve sampling.
The ratio of accepted moves to attempted moves determined with the frequency specified by the keyword *acceptatmmoveupdate*.
In addition, the ratio of accepted to attempted moves is controlled by *acceptatmmoveratio* and by default is set to 0.37.  
This value is generally believed to deliver optimum sampling but with DL_MONTE you can explore whether this is the case for your system.

Try a number of values and investigate how the energy equilibrates during the course of the simulation.
You can use::

   grep displacement OUTPUT.000

or the script::

   disp.sh

to print the initial values and the final value of the maximum displacement(s).

N.B.  This functionality should be used to identify the optimum move size for sampling a given system.  Beware using this functionality in a calculation as it can break detailed balance.

Move to :ref:`tutorial_2`

:ref:`index`
