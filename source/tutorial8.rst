.. _tut8:

TUTORIAL 8 : Planar geometry confinement
========================================

:Author: Andrey V. Brukhno (andrey.brukhno{at}stfc.ac.uk)

Illustration of a confined system
---------------------------------

In this tutorail we demonstrate how to set up a simulation for a system confined to a planar nano-pore.

.. figure:: ./images/tutorial8-slit-mono.png
   :width: 450px


Two types of quasi-2d constraints
---------------------------------

DL_MONTE supports two types of setup for constrained systems:

* A '**slit**' constraint where the system is non-periodic in z-direction, so that it is confined between two flat, impenetrable walls (infinite in x and y). 
  
  To invoke the slit constraint one has to add the following directive in the *CONTROL* file (after 'finish use')::
  
   slit walls hard     # both walls are set to be hard (impenetrable)
  
  One or both walls can be defined as 'soft'::
   
   slit walls soft 1   # one of the walls is set as soft (at the bottom, -Z/2)
   #slit walls soft 2   # two walls are set as soft (-Z/2, Z/2)
  
  Soft walls *must* be assigned an external potential to interact with one or more atomic types, which is specified in the *FIELD* file, for example::
  
   EXTERNAL 2 # wall interactions: LJ epsilon & sigma
   OW  core   lj  0.90  3.00
   HW  core   lj  0.00  0.01
   
  (see DL_MONTE manual for details of the available external potentials).
  
  This setup is applicable to systems that do not contain charges (i.e. without electrostatics).
  
  **If charges are present, Coulomb interations require a long-range correction in 2D PBC!** 
  
  The correction can be included by employing a mean-field approximation (MFA), which accounts for the Coulomb interations of all the *charges within* the simulation cell with the *charge density distribution outside* the primary cell. The MFA treatment is enabled in the *CONTROL* file (in addition to the above directives)::
  
   slit mfa  -1 100 0.25   # MFA correction, starting from scratch, 100 iterations, density mixing factor
  
  (see DL_MONTE manual for more details).
  
  Surface charge density (SCD) on one or both walls can also be included::
   
   slit walls charged  2  -0.01  0.01  # both walls are charged, SCD1 = -0.01, SCD2 = 0.01 e-/A^2
 
  **NOTE:** The MFA correction works best for systems with `implicit solvent' where the dielectric permittivity is high and, therefore, the long-range correction is relatively small. 
  
* An `extended slit' constraint, known as a '**slab**' setup, where the simulation cell is extended in Z direction with a vaccum layer filling in the space beyond the slit walls::

   slit walls soft 2  zfrac2 0.2 # two soft walls shifted to +/-0.2*(Z/2)

  where '*zfrac2 = 0.2*' determines the slit *Z-fraction* of the simulation cell (occupied by the system), while the rest of the cell is empty (filled in with vacuum).

  In this case, providing that the vacuum layer is sufficiently thick, the long-ranged electrostatic interactions can be calculated by use of the Ewald summation in full 3D PBC.

Exercises:
----------

  :ref:`tut8_ex1` - Ideal gas partitioning in a planar pore

  :ref:`tut8_ex2` - Water adsoprtion in a slit

