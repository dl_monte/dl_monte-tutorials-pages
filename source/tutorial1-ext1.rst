.. _tut1_ex1:

TUTORIAL 1 : Exercise 1 
=======================

Ex. 1.1 Random Seeding
----------------------

Edit the CONTROL file to use a random number seed.

Replace the line reading:

.. code-block:: html

   seeds int1 int2 int3 int4

so that it reads:

.. code-block:: html

   ranseed 

Re-run your calculation and verify that the trajectory is different.
The trajectory is still repeatable as DL_MONTE includes the randomly generated seeds in OUTPUT.000.
Locate these using::

   grep seeds OUTPUT.000

and re-run the calculation with these as seeds to verify the calculation is reproducible.

N.B. Random seeding works by generating initial random seeds from the system clock.  
If you are launching multiple instances of DL_MONTE in a trivially parallel fashion this could lead to all starting with the same random number seeds.  
To avoid this you can should ensure time separation between starting calculations, e.g. using the command::

   sleep x

Continue with :ref:`tut1_ex2`

Move to :ref:`tutorial_2`

:ref:`index`
