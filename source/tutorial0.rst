.. _tutorial_0:

.. TU*: Comments/to-do
   @ The installation information could be give options for serial and parallel compilation separately,
     since a lot of users may not have MPI on their, e.g. laptops? The build-all script builds everything, but
     will fail if MPI is not installed.
   @ There is a typo below in the use of build-all; the command given is wrong.
   @ The information describing contributions to DL_MONTE could be updated: now the procedure would be to use
     GitLab to make a merge request
   @ The formatting for the 'Generic DL_MONTE Workflow section' could be improved; the lists and indents don't
     come out quite right in the html

   
Introduction and set up
=======================

:Authors: Andrey V. Brukhno (andrey.brukhno@stfc.ac.uk), John Purton (john.purton@stfc.ac.uk), and Tom L. Underwood (t.l.underwood@bath.ac.uk)


Introduction
------------

Welcome to the DL_MONTE tutorials!
Here we provide some information which is required before the DL_MONTE tutorials can be undertaken in earnest, namely:

* Essential information about DL_MONTE
* How to obtain, install and run DL_MONTE
* How to obtain the files required for the DL_MONTE tutorials
* How to set up your shell environment in preparation for undertaking the tutorials

Note that we assume in these tutorials that you are competent in using the unix shell; the unix shell will be the environment in which we
will be performing DL_MONTE simulations and analysing their output. If you are not competent at using the shell, then there are a number
of excellent tutorials online which can be used to sharpen your skills.


About DL_MONTE
--------------

DL_MONTE is a *state of the art* parallel Monte Carlo simulation package aiming to provide the academic community with a general purpose advanced MC engine. The development of DL_MONTE-2 has been instigated by the EPSRC’s Software for the Future programme (research grant EP/M011291/1).

The `DL_MONTE GitLab page <https://gitlab.com/dl_monte>`_ contains extensive information about DL_MONTE, including links to training material
such as these tutorials and a `suite of examples <https://gitlab.com/dl_monte/dl_monte-examples>`_. Moreover, the DL_MONTE manual is also an
essential resource. A manual is always provided with a DL_MONTE release (see below). Alternatively, a link to the latest version of the
manual can be found `here <https://gitlab.com/dl_monte/user-hub/-/wikis/uploads/e124593c1040cb412aa1fefc73b723be/DL_MONTE-206_Manual.pdf>`_.

License
^^^^^^^

DL_MONTE package is provided as an open source code under the `BSD license <http://www.linfo.org/bsdlicense.html>`_. In brief, this implies that anyone registered with the project can obtain the source files, modify them for their own use, but they are not allowed to distribute the software without a written permission from the Sci-Tech Daresbury, STFC.



User feedback
^^^^^^^^^^^^^

Users' constructive feedback is very much appreciated by the development team, as it helps us to debug the program in various possible scenarios which
we might not have encountered ourselves. If you find that DL_MONTE behaves not as described in the manual or produces erroneous results for a specific
system, or if you find a definite *bug* in the source code, we would be very grateful for your sending us a sufficiently detailed report on the issue,
including the input and output files that would allow us to reproduce it and debug the code. 

Contributing to DL_MONTE
^^^^^^^^^^^^^^^^^^^^^^^^

If you are interested in actively facilitating prioritised development of a methodology that is not available in DL_MONTE, the best practice is to
initiate a dialogue with the development team. Should the feature be approved for accelerated development, it is expected that this should take the
form of a collaborative effort (within the spirit of the CCP5 remit) between the proposer and the development team.

Finally, if you are a seasoned programmer who is extending the functionality in DL_MONTE on their own, and wish to contribute to the DL_MONTE
repository, you need to send us a compressed archive (either *tar.gz* or *zip* file) including at least two directories: one containing your local,
thoroughly tested, version of the code, and the other with working examples (input and output files) which could be used for benchmarking and
validating your patch. Providing your extention code is written clearly, and its structure and formatting follows our programmer's guide, we will
consider your contribution for inclusion in the main DL_MONTE repository.



How to obtain DL_MONTE
----------------------

The following steps should be performed to obtain the latest release of DL_MONTE

1. Register as a user via the CCP5 web pages at https://ccp5.ac.uk/DL_MONTE by clicking the REGISTRATION link.
   This will provide you with a password to decrypt the archive file containing the latest release.

2. Download the latest release from the DL_MONTE GitLab page at https://gitlab.com/dl_monte/dl_monte-releases

2. Unpack the *zip* file into a suitable directory e.g. dlmonte_release_pack

Installing DL_MONTE
-------------------

DL_MONTE compilation requires a suitable FORTRAN compiler such as *gfortran* and an MPI (mpich or OpenMPI) library pre-installed.
First, navigate into the directory containing the source files:
::

   [software_dir]$ cd dlmonte_release_pack

Now you can use the script called **build:** using the command.

.. code-block:: html

   [dlmonte_release_pack]$ ./build all:

Upon successful compilation the executable(s) are saved in subdirectory **bin** (within dlmonte_release_pack), which you can check::

   [dlmonte_release_pack]$ ls ./bin

   DLMONTE-PRL-VDW_dir.X DLMONTE-PRL-VDW_tab.X DLMONTE-PRL.X DLMONTE-SRL-VDW_dir.X DLMONTE-SRL-VDW_tab.X DLMONTE-SRL.X

where the executable names indicate the compilation mode used for each particular executable. PRL/SRL indicates parallel/serial,
opt/dbg optimised/debug executables and VDW_dir/VDW_tab for the calculation mode of the Van der Waal interactions.

The files without the VdW suffix are the most recent compiled versions.

HINT: In order to avoid copying executable(s) into each directory from where a simulation will be run, the user is advised to
store executables in a location that is included in the environment variable PATH (e.g. *$HOME/bin/*)::

   [dlmonte_release_pack]$ cp  ./bin/DLMONTE-PRL.X  ./bin/DLMONTE-SRL.X  ~/bin/

or, alternatively, add the full path to DL_MONTE executables at the end of $PATH variable. 

This way the executables can be found and launched from any location.


Running DL_MONTE
----------------

 1.1 Create a subdirectory for your simulation run (in a dedicated directory for DL_MONTE simulations)::

   [dlmonte_sims]$ mkdir simulation_dir

 **NOTE: We strongly advise against running simulations from within the source directory**.

 1.2 Navigate to that directory and prepare (or copy) three *compulsory input files*: CONTROL, CONFIG and FIELD in the directory for your simulation. 

 2.1 Launching a serial executable is straightforward::

   [simulation_dir]$ DLMONTE-SRL.X

 HINT: Adding **&** symbol at the end of the command will send the execution into *background* and, hence, release the command line (i.e. the terminal)
 for further input. Otherwise one cannot use the command line (the terminal) and has to wait until the job is finished.

 2.2 To launch an MPI-linked executable, one has to use the following command:: 

   [simulation_dir]$ mpirun -np # DLMONTE-PRL.X 

 where *#* (hash) stands for the number of MPI threads (or CPU cores) to use.

NOTE: The above commands are only suitable for launchning simulation jobs locally on user's desktop or laptop. When launching jobs on a remote HPC facility (e.g. a Beowulf cluster), one has to refer to the job submission guidance for the particular HPC environment (which should describe the specifics of submitting jobs to a queue system).

3. If your job finished successfully, i.e. did not terminate abnormally, a number of output files will be found in the directory from where the executable was launched.

 Typically, a DL_MONTE simulation will produce the following output files (the list below is not exhaustive):

* OUTPUT.000 -- details of the simulation, statistics etc (this is the file to check for warnings and errors).
* PTFILE.000 -- statistics stored by frame with a specified stride (in the legacy format standard for DL_MONTE-1)
* YAMLDAT.000 -- statistics stored by frame with a specified stride (in the YAML format standard for DLMONTE-2)
* REVCON.000 -- the final or most recent configuration stored with a specified stride (see DL_MONTE manual)
* A trajectory file in at least one of the following formats:

  ARCHIVE.000 -- native DL_MONTE format (each frame is stored in the same format as that of CONFIG file)

  HISTORY.000 -- compatible with DL_POLY format (either DL_POLY-2 or DL_POLY-4, see DL_MONTE manual)

  TRAJECTORY.000 -- DCD format recognised by VMD visualisation software (compatible with CHARMM and NAMD)

4. Analyse the data using the helper scripts or the `dlmontepython Python toolkit <https://gitlab.com/dl_monte/dlmontepython>`_ (see the exercises).


Obtaining the tutorial files
----------------------------

The input files and helper scripts can be downloaded as an archive from the DL_MONTE GitLab page at https://gitlab.com/dl_monte/dl_monte-tutorials-files. 
The archive should be downloaded and extracted.

The arcive contains two directories, **exercises** and **scripts**.

A brief tour of the tutorial files
----------------------------------

The DL_MONTE input files and sample output is provided in the **exercises** directory:
::

    [DL_MONTE-2_tutorial]$ cd exercises
    [DL_MONTE-2_tutorial]$ ls

    tutorial_1  tutorial_2  tutorial_3  tutorial_4  tutorial_5  tutorial_6  tutorial_7

In these tutorials we will be using the input files in the subdirectories within **exercises**.
That is, we will be navigating to each particular subdirectory to run the corresponding example simulation. 
So **simulation_dir** above stands for the actual path to the input files. 

Scripts to perform data analysis are provided in the **scripts** directory.


Configuring your environment for the tutorials
----------------------------------------------

To make life easier, we want to be able to 'see' the data analysis scripts from any directory. Hence ,
before we proceed to actual simulations, copy the scripts directory to your $HOME/bin:
::

  [DL_MONTE-2_tutorial]$ cp -rv scripts $HOME/bin/

If this command fails because you do not have a directory named $HOME/bin then you should create one
and try again:
::

  [DL_MONTE-2_tutorial]$ mkdir $HOME/bin
  [DL_MONTE-2_tutorial]$ cp -rv scripts $HOME/bin/
  
Then you should make it visible via the $PATH environment variable:
::

  [DL_MONTE-2_tutorial]$ export PATH="$HOME/bin/scripts":$PATH

Finally, check if everything has been in place:
::

  [DL_MONTE-2_tutorial]$ ls ~/bin/DL_MONTE*
  [DL_MONTE-2_tutorial]$ ls ~/bin/scripts


