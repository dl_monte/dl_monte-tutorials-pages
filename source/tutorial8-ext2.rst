.. _tut8_ex2:

TUTORIAL 8 : Exercise 2 
=======================

Water adsorption
----------------

In this exercise we will use GCMC to simulate water adsorption into a nano-pore.

Navigate to directory **tutorial_8/slit-water** ...

Check the FIELD file::

   slit-water > more FIELD

The important points to note in the FIELD file are: 
 
 the energy *unit* is *kJ/mol* (real units) 
 
 *ATOMS 3 3* and 3 lines that follow provide the molecule template (for GCMC)

 *EXCLUDE* tells DL_MONTE not to include any intra-molecular interactions(!)
 
 *RIGID* tells DL_MONTE to consider the molecule as a 'rigid body'

 *VDW 3 shift* implies 3 definitions for short-range interactions, truncated and shifted at cutoff(!)

 the *EXTERNAL* wall-atom *lj* interactions are at play
 
 *epsilon = 9.0* kJ/mol and *sigma = 3.2* Angstrom (from a wall) for *OW* atom type.

Check the CONTROL file::

   slit-water > more CONTROL

The important directives to note in the CONTROL file are::

  use gaspressure
  use rotquaternion
  use ortho
  finish

  slit walls soft 2  #z-move -0.8
  shifted damped 0.320
  
  sample zdensity  500  1000
  
  revconformat  dlpoly2
  archiveformat dlpoly2+dcd
  
  move gcinsertmol 1 50 2.5
  SPCE 0.002

which instruct DL_MONTE to 

  use *partial gas pressure* in the GCMC (insertion/deletion)sampling;
  
  use *quaternion* representation for molecule rotations;
  
  use *orthogonal* variant of PBC (optimized for orthorhombic cells)

  treat the system in a palnar *slit* geometry with two *soft walls* (not a 3D 'slab'!)
  
  employ a *force-shifted and damped* (truncated) variant of Coulomb interations (no Ewald!)
  
  *sample zdensity* on a grid with 500 bins every 1000 MC steps (iterations)
  
  store the REVCON file in both DL_MONTE (default) and DL_POLY-2 (additional) formats
 
  store the trajectory in the following formats: *dlpoly2* & *dcd*

  perform GCMC *moves* (gcinsertmol) for 1 molecule type with weight 50(%) and *min.distance = 2.5* A 
  
  the partial gas pressure for *SPCE* molecule is set to 0.002


Run the simulation and, when done, plot the density profile along z (ZDENSITY.000)::

   [tutorial_8]$ gnuplot
   gnuplot> plot [x=-6:6] [y=0.:3.0] 'ZDENSY.000' u 1:2 w l t "Density(z)"

.. figure:: /images/tutorial8_SPCE-ZDENSY.png
   :width: 650px

Questions to ask yourself:
^^^^^^^^^^^^^^^^^^^^^^^^^^

* How does the external potential look like?
* How to increase or decrease the water adsoprtion? - What parameter(s) you could vary?
* What will happen if you alter the external potential strength or distance?
* What will happen if you alter the partial pressure for GCMC?
* What will happen if you increase the cutoff radius?
* What would be a good measure of the water adsorption?

If we increase the 'steps' number in CONTROL by a factor of 5 (uncomment the extra zero), we get slightly better results, as below.

.. figure:: ./images/tutorial8_SPCE-ZDENSY2.png
   :width: 650px

Additional exercise:
^^^^^^^^^^^^^^^^^^^^

Try to vary the external potential, and then partial pressure (gradually) and see the effect.
Use VMD to visualise the trajectories.

Previous exercise:
------------------

  :ref:`tut8_ex1` - Ideal gas partitioning in a planar pore

