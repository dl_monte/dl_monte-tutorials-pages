
.. TU:
   @ TO DO: Brush up the exit section: the job is in the background, so the instructions here are wrong
   @ TO DO: Finish after BREAK
   @ TO DO: Need to copy across DL_MONTE input files for the tutorials also
   
Introduction to the dlmontepython tutorials [Bath workshop]
===============================================================

:Authors: James Grant (r.j.grant@bath.ac.uk), and Tom L. Underwood (t.l.underwood@bath.ac.uk)

Introdution
-----------

dlmontepython is a Python package comprised of various Python modules to assist with setting up, running, and analysing the output of
DL_MONTE simulations. Here we provide a little information about dlmontepython, and describe how to run the dlmontepython tutorials on Balena.

Prerequisites
--------------

It is assumed here that you have already configured your environment on Balena by following the instructions in :ref:`tutorial_0_bath`.


About dlmontepython
-------------------

dlmontepython is a Python package comprised of various Python modules to assist with setting up, running, and analysing the output of
DL_MONTE simulations.
dlmontepython is currently hosted on the DL_MONTE GitLab webpage `here <https://gitlab.com/dl_monte/dlmontepython>`_, from where the
latest version can be downloaded.

You may have noticed that, unlike the DL_MONTE source code, dlmontepython can be obtained without registration.
Moreover, there are no named releases of dlmontepython. This is due to the fact that dlmontepython is fairly new; we have yet to decide
on a release strategy.

About the tutorials
^^^^^^^^^^^^^^^^^^^

Included with dlmontepython are example scripts, as well as tutorials in the form of Jupyter Notebook documents.
`Jupyter Notebook <https://jupyter.org/>`_ is an application which can be run in your web browser which "allows you to create
create and share documents that contain live code, equations, visualizations and narrative text". Jupyter notebook documents
(henceforth refered to simply as 'notebooks') are an excellent medium for creating tutorials for Python code, among other things.

Below we describe how to run notebooks, specifically the dlmontepython tutorials, on Balena.

License
^^^^^^^

dlmontepython is provided as an open source code under the `BSD license <http://www.linfo.org/bsdlicense.html>`_. In brief, this implies that anyone can obtain the source files and modify them for their own use, but they are not allowed to distribute the software without a written permission from the Sci-Tech Daresbury, STFC.


Contributing to dlmontepython
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

We welcome contributions to dlmontepython. Please contact the development team if you are interested in doing so.


Finding the tutorials
---------------------

On Balena, your environment has been configured so that the environment variable *DLMONTEPYTHON* is the directory containing
the dlmontepython package.

::
   
   [user@host.domain ~]$ ls $DLMONTEPYTHON
   dlm_wham.py  docs  examples  fep  htk  __init__.py  LICENSE  MANIFEST.in  __pycache__  README.md  setup.py  simtask

The *docs* subdirectory contains the notebook tutorials. Note that there is a set of tutorials for th *htk* package, and
a tutorial for the *simtask* package:

::
   
   [user@host.domain ~]$ ls $DLMONTEPYTHON/docs/htk/tutorial/
   intro-dlmonte.ipynb  intro-ising.ipynb   util-dlmonte.html   util-dlmonte_with_output.html
   intro-ising.html     util-dlmonte_files  util-dlmonte.ipynb
   [user@host.domain ~]$ ls $DLMONTEPYTHON/docs/simtask/tutorials/
   CONFIG  CONTROL  dlmonte_gcmc.ipynb  FIELD

The files ending in *.ipynb* are the notebooks.

Before opening a notebook, we should make a local copy of the notebook and associated files, since during the tutorial we will modify the notebook. We begin with
the *util-dlmonte.ipynb* tutorial:

::
   
   [user@host.domain ~]$ cd scratch
   [user@host.domain scratch]$ mkdir htk
   [user@host.domain scratch]$ cd htk
   [user@host.domain htk]$ cp -r $DLMONTEPYTHON/docs/htk/tutorial/util-dlmonte.ipynb util-dlmonte_files .

For the *simtask* tutorial the relevant commands are:

::

   [user@host.domain scratch]$ mkdir simtask
   [user@host.domain scratch]$ cd simtask
   [user@host.domain simtask]$ cp -r $DLMONTEPYTHON/docs/simtask/tutorials/* .


Running a notebook
------------------

We will now demonstrate how to run a notebook on Balena; we will run the *util-dlmonte.ipynb* tutorial you copied to scratch
above.

Firstly, we launch an interactive session on balena:

::
   
   [user@host.domain htk]$ sint
   salloc: Granted job allocation 2471707
   srun: Step created for job 2471707
   [user@intnode htk]$

Recall this logs us onto one of the compute nodes of Balena, from where we can perform simulations, or in this case run a notebook,
interactively.

Next, we must configure our environment by loading the appropriate module:

::
   
   [user@intnode htk]$ module load python3/2019.x

Then we must configure Jupyter by running the following commands. Note that these need only be run once; the second, third, etc. time
you try to run a notebook it is unnecessary to run these commands.

::
   
   [user@intnode htk]$ jupyter notebook --generate-config
   [user@intnode htk]$ jupyter notebook password

The second of these commands will prompt you to set up a password. This password is specific to running Jupyter notebooks on Balena, and
should probably not be the same as the password for your computing account. We will use this password in a moment, so keep it in your
mind!

Next, we will obtain the IP address of the compute node running your interactive session:

::
   
   [user@intnode htk]$ host $(hostname)
   intnode.cm.balena has address 123.45.6.78

In this example the IP address is 123.45.6.78. Keep this IP address at hand; we will use it in a moment.

Now, launch the Jupyter Notebook program using the following command:

::
   
   [user@intnode htk]$ jupyter notebook --ip='*' --no-browser &

After a moment some text should appear in the terminal indicating that the program is running.

Jupyter Notebook is now running on Balena. However, recall earlier we said that a web browser is used to view notebook documents.
With this in mind, open a web browser (on the computer lab machine - not on Balena!). Once you have done this, go to the web address

::
   
   http://[ipaddress]:8888

where *[ipadress]* is the IP address of the interactive compute node on Balena you noted above. At this address you will be prompted
for your Jupyter password, which you should enter. Once you do this you should be able to are presented with the contents of the
directory you are in on the interactive compute node, one of which is the *util-dlmonte.ipynb*. You can open the notebook by clicking
on it.

Exiting
-------

To exit click the 'Logout' button at the top right corner in the web browser. Then, in the command line terminal for your interactive
session on Balena, hit CTR+C twice to kill the Jupyter Notebook program. Finally, you can exit your interactive session on Balena
by using the *exit* command in the terminal.
