.. _tut8_ex1:

TUTORIAL 8 : Exercise 1 
=======================

Ideal gas in a planar pore
--------------------------------------

In this exercise we will simulate ideal gas partitioning in a slit confinement, in either "non-wetting" or "wetting" surface conditions.

Navigate to directory **tutorial_8/slit-ideal** ...

Check the FIELD file::

   slit-ideal > more FIELD

The initial FIELD file for this tutorial should look as shown below.

.. code-block:: html
   :linenos:

   Ideal gas in a slit
   CUTOFF 1.0
   UNITS K
   NCONFIGS 1
   ATOMS 1
   LJ core 1.0  0.0
   MOLTYPES 1
   IG
   MAXATOM 256
   FINISH
   VDW 1
   LJ core  LJ core  hs   0.0  0.0
   external 1
   LJ core  hs       0.0  1.0  1.0
   CLOSE
   
The important points to note are: 

 the *cutoff = 1.0* (very small)
 
 the energy *unit* is *K* (reduced units) 
 
 the particle-particle *hard sphere* interaction is *zero* (ideal gas!)

 the *external* particle-wall *hard square* interaction is in the form of a 'repulsive shoulder': 
 
 *width = 1.0* Angstrom (from a wall) and *strength = 1.0* kT. 

Check the CONTROL file::

   slit-ideal > more CONTROL

The initial CONTROL file for this tutorial is

.. code-block:: html
   :linenos:
   
   NVT simulation of ideal gas in the slit geometry 
   use ortho
   finish
   seeds 12 34 56 78               # Seed RNG seeds explicitly to the default                          

   slit walls soft 2 
   #slit mfa 0

   noewald all
   temperature       1.0           # Corresponds to T*=1.0
   pressure          1.0           # katm (*10^9)

   steps           1000000 #0         # Number of moves to perform in simulation
   equilibration       0           # Equilibration period: statistics are gathered after this period
   print            100000         # Print statistics every 'print' moves
   stack              1000         # Size of blocks for block averaging to obtain statistics

   sample zdensity    120  10
   sample coordinates 100000

   archiveformat    dlpoly2 #+dcd
   revconformat     dlpoly2        # REVCON file is in DL_POLY CONFIG format

   acceptatmmoveupdate   100000000

   maxatmdist  10.0
   move atom 1  100               # Move atoms 100% of the time
   LJ core

   check   1000000
   start 

Run the simulation and, when done, plot the density profile along z (ZDENSITY.000)::

   [tutorial_8]$ gnuplot
   gnuplot> plot [x=-6:6] [y=0.:3.0] 'ZDENSY.000' u 1:2 w l t "Density(z)"

.. figure:: ./images/tutorial8-slit-ig0.png
   :width: 640px

Questions to ask yourself:
^^^^^^^^^^^^^^^^^^^^^^^^^^

* Is it "wetting" or "non-wetting" case?
* Why are there two levels in density?
* Can you calculate the exact density levels? 
* How does the external potential look like?
* How to reconstruct the external potential (approximately) from the density data?
* What will happen if you increase the cell dimentions in X and Y in CONFIG?

If we increase the 'steps' number in CONTROL by a factor of 10 (uncomment the extra zero), we get a much better precision, as below.

.. figure:: ./images/IG-256p-rep1kT1A.png
   :width: 900px

Additional exercise:
^^^^^^^^^^^^^^^^^^^^

Change the sign for the external interaction *strength* (in FIELD), and rerun the simulation.

* How do you think the density will look this time? 

-------

**Hint:** :math:`\rho(z) = \rho(0)\exp(-\beta U(z))`

Next exercise:
--------------

  :ref:`tut8_ex2` - Water adsoprtion in a slit

