.. _tut2_ex2:

TUTORIAL 2 : Exercise 2 
=======================

Target acceptance ratio
------------------------

The ratio of accepted volume moves is controlled by *max_vol_change* in a similar manner to the atom displacements.
The is initially set in DL_MONTE by the keyword *maxvolchange <change/2>*. 
During the simulation the value is automatically adjusted in DL_MONTE as for the atoms. 
*acceptvolupdate <frequency>* determines the frequency of the calculation to adjust the maximum volume change and *acceptvolratio <target ratio>* is the desired ratio of accepted to rejected volume displacements.
Introduce the directives to your calculation and evaluate how they affect sampling.

The script::

  volmoves.sh

will extract the information detailing the volume moves from OUTPUT.000.

Further exercises
-----------------

Move on to  :ref:`tutorial_3` and learn how to obtain radial distribution functions (RDF).

