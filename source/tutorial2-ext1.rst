.. _tut2_ex1:

TUTORIAL 2 : Exercise 1 
=======================

Alternative V-sampling schemes
------------------------------

It is also possible to conduct a random walk in the logarithm of the volume, by using the keyword **log** instead of **linear** in the directive **move volume**. This changes the way in which the move is generated: :math:`\ln(V)` is varied rather than linear dimensions of the simulation cell. This type of V-sampling is often claimed to be more efficient due to direct *scaling* of volume. 

The acceptance probability for the volume moves is now

.. math::

  P_{\mathrm{acc}}([\mathbf{r}_{1},V_1] \rightarrow [\mathbf{r}_2,V_2]) = \min(1, \exp \{- \beta [U(\mathbf{r}_2) - U(\mathbf{r}_1) + P_{ext}(V_{2}-V_{1}) - ( N + 1 ) \beta^{-1} \ln(V_{2} / V_{1}) ] \} )

Often simulation cells will be orthorhombic (but not cubic), and more generally mono-/tri-clinic. For setting a more general volume move in these cases refer to the manual.
         
The fraction of accepted volume moves is affected by the directive **maxvolchange** setting the value of the *volume variation parameter* :math:`\Delta_V`. In the course of an NpT simulation this value is though automatically adjusted so that the acceptance ratio converges to the targeted value that can be set in the CONTROL file by the directive **acceptatmmoveratio <target ratio>** (0.37 by default). Additional directive **acceptvolupdate <stride>** determines the frequency of adjustments for :math:`\Delta_V`. 

Exercise
--------

Introduce the above directives in the CONTROL file and evaluate how they affect V-sampling and the outputs.

For doing so, create a separate subdirectory and copy the input files therein.

Also check out :ref:`tut2_ex2`.

Or move on to  :ref:`tutorial_3` and learn how to obtain radial distribution functions (RDF).

