.. _tut1_ex2:

TUTORIAL 1 : Exercise 2 
=======================

Ex. 1.2 Neighbour lists
-----------------------

DL_MONTE uses Verlet neighbour list to improve the performance of the energy calculations, so that particles only have to check interactions with their neighbours, not every particle in the simulation. This is particularly beneficial when particles retain the same numbers for the whole simulation.

The key directives are::

   nbrlist auto

This rebuilds a particle's neighbourlist whenever necessary, beware changing this.
The size of the neighbourlist is determined by::

   maxnonbondnbrs <int>

This determines the memory allocated for each particles neighbourlist.
The size will be determined by the size of your system, its density and the interaction cut-off as specified in the FIELD file.  
Most performance benefit will be obtained by tuning the directive::

   verlet <float>

which is set to 2.0 by default.
Try different values for this system, the time taken for the calculation can be found using the command::

   grep "total elapsed" OUTPUT.000

or alternatively the script::

   time.sh

How does tuning the parameter affect the duration of this calculation?  Why might this be the case?

N.B. For short simulations system time can dominate the apparent performance of the calculation.  To see this affect try running DL_MONTE consecutively and checking the duration.

Continue with :ref:`tut1_ex3`

Move to :ref:`tutorial_2`

:ref:`index`
